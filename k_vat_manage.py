from flask import Flask, render_template, request, redirect, url_for, session,make_response,flash
from app import app
from dbconn import *
import psycopg2 as p
from datetime import datetime, date
import pdfkit
import requests
from dbconn import *
from dbconAPIMGT import *



@app.route('/manage')
def manage():
    # link = "http://150.95.90.124:4433/emppost"
    # f = requests.get(link)
    # print(f.text)
    return render_template('manage/index.html')



@app.route('/typeincome')
def typeincome():
    with getcursor(session.get("database")) as cur:
        sql = "select code,name_1 from tb_type_incom"
        cur.execute(sql)
        listitem = cur.fetchall()
    return render_template('manage/typeincom.html',listitem=listitem)


@app.route('/Insert',methods=['POST'])
def Insert(): 
    if not session.get("name"):
        return redirect("/login")
    try:  
        with getcursor(session.get("database")) as cur:
            code = request.form['code']
            name_1 = request.form['name_1']
            sql_check = "select count(code) from public.tb_type_incom where code=%s"
            data_new = (code,)
            cur.execute(sql_check, (data_new))
            check = cur.fetchone()
            print(check,'||',check[0])
            if check[0] == 0:
                sql = "INSERT INTO tb_type_incom(code, name_1)VALUES (%s, %s);"
                data = (code,name_1)
                cur.execute(sql, (data))
                flash ('ເພີ່ມຂໍ້ມູນສຳເລັດ')
            else:
                flash ('ລະຫັດນີ້ມີແລ້ວ')
    except Exception as e:
        print("error in executing with exception: ", e)
    return redirect(url_for('typeincome'))


@app.route('/update',methods=['POST'])
def update():
    if not session.get("name"):
        return redirect("/login")
    try: 
        with getcursor(session.get("database")) as cur:
                code = request.form['code']
                name_1 = request.form['name_1']            
                sql = "update tb_type_incom set name_1=%s where code=%s "
                data = (name_1,code)
                cur.execute(sql, (data))
                flash ('ແກ້ໄຂສຳເລັດ')
    except Exception as e:
        print("error in executing with exception: ", e)
    return redirect(url_for('typeincome'))

@app.route('/delete/<id>')
def delete(id):
    if not session.get("name"):
        return redirect("/login")
    try: 
        with getcursor(session.get("database")) as cur:        
                sql = "DELETE FROM tb_type_incom where code=%s "
                cur.execute(sql, (id,))
                flash ('ຂໍ້ມູນຖືກລົບແລ້ວ')
    except Exception as e:
        print("error in executing with exception: ", e)
    return redirect(url_for('typeincome'))