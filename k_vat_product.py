from flask import Flask, render_template, request, redirect, url_for, session,make_response,flash
from app import app
from dbconn import *
from datetime import datetime, date


@app.route('/product')
def product():
    if not session.get("name"):
            return redirect("/login") 
    with getcursor(session.get("database")) as cur:
        cur.execute("""SELECT row_number() OVER () as rnum,code,name_1,name_2,unit_code,item_type,tax_type,
        case when tax_type = 0 then 'ອມພ 0'
        when tax_type = 1 then 'ອມພແຍກນອກ'
        when tax_type = 2 then 'ອມພລວມໃນ'
        when tax_type = 3 then 'ຍົກເວັ້ນ ອມພ'
        else '' end as tax_type
        FROM ic_inventory order by roworder asc""")
        data = cur.fetchall()

        if session.get("database") == 'sabaideevat':
            cur.execute("SELECT max(code) FROM ic_inventory ")
            ar_id = cur.fetchone()
            if ar_id[0]:
                pro_code=1+int(ar_id[0])
            else:
                pro_code='10001'
        else:
            pro_code=''

        cur.execute("""SELECT code,name_1 from ic_unit order by roworder asc""")
        unit = cur.fetchall()
    return render_template('Product/index.html',data=data,unit=unit,title=session.get("database"),pro_code=pro_code)
@app.route('/save_product',methods=['POSt'])
def save_product():
    if not session.get("name"):
            return redirect("/login") 
    try:
        with getcursor(session.get("database")) as cur:
            code = request.form['code']
            name_1 = request.form['name_1']
            name_2 = request.form['name_2']
            product_type = request.form['product_type']
            vat_type = request.form['vat_type']
            unit_code = request.form['unit_code']

            sql = "INSERT INTO public.ic_inventory(code, name_1, name_2, unit_code,item_type, tax_type)VALUES  (%s, %s,%s,%s,%s,%s);"
            data = (code, name_1, name_2, unit_code,product_type,vat_type)
            cur.execute(sql, (data))
    except Exception as e:
        print("error in executing with exception: ", e)
    return redirect(url_for('product'))


@app.route('/update_product',methods=['POSt'])
def update_product():
    if not session.get("name"):
            return redirect("/login") 
    try:
        with getcursor(session.get("database")) as cur:
            code = request.form['code']
            name_1 = request.form['name_1']
            name_2 = request.form['name_2']
            product_type = request.form['product_type']
            vat_type = request.form['vat_type']
            unit_code = request.form['unit_code']
            sql ="update  ic_inventory set name_1=%s, name_2=%s, unit_code=%s,item_type=%s, tax_type=%s where code=%s"
            data = ( name_1, name_2, unit_code,product_type,vat_type,code)
            cur.execute(sql, (data))
    except Exception as e:
        print("error in executing with exception: ", e)
    return redirect(url_for('product'))



@app.route('/deleteproduct/<id>' ,methods=['DELETE'])
def deleteproduct(id):
    with getcursor(session.get("database")) as cur:
       cur.execute("delete from ic_inventory  where code=%s",(id,))
    return '', 200

#unit product
@app.route('/unit')
def unit():
    if not session.get("name"):
            return redirect("/login") 
    with getcursor(session.get("database")) as cur:
        cur.execute("""
        SELECT row_number() OVER () as rnum,code,name_1,(SELECT count(code) FROM ic_unit WHERE code in (SELECT unit_code FROM ic_inventory) and code=a.code),roworder
        FROM ic_unit a order by roworder asc""")
        data = cur.fetchall()

    return render_template('Product/unit.html',data=data,title=session.get("database"),)

@app.route('/save_unit',methods=['POSt'])
def save_unit():
    if not session.get("name"):
            return redirect("/login") 
    try:
        with getcursor(session.get("database")) as cur:
            name_1 = request.form['name_1']

            cur.execute("""SELECT count(code) FROM ic_unit where code=%s""",(name_1,))
            get_check = cur.fetchone()

            if get_check[0] == 0:
                sql = "INSERT INTO ic_unit(code, name_1)VALUES (%s, %s)"
                data = (name_1, name_1,)
                cur.execute(sql, (data))
                flash('ສຳເລັດ')
                return redirect(url_for('unit'))
            else:
                flash('ບໍ່ສາມາດເພີ່ມໄດ້ ມີໃນລະບົບເເລ້ວ')
                return redirect(url_for('unit'))

    except Exception as e:
        print("error in executing with exception: ", e)
    return redirect(url_for('unit'))

@app.route('/deleteunit/<id>' ,methods=['DELETE'])
def deleteunit(id):
    with getcursor(session.get("database")) as cur:
       cur.execute("delete from ic_unit  where roworder=%s",(id,))
    return '', 200



