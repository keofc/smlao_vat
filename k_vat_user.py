from flask import Flask, render_template, request, redirect, url_for, session
from app import app
from dbconMGT import *
from flask.helpers import flash


#view user
@app.route('/user')
def user():
    if not session.get("name"):
            return redirect("/login") 
    try:
        with getcursorSMLAO() as cur:
                sqla="select  row_number() OVER () as rnum,code,name_1,username,password,roles from smlao_user"
                cur.execute(sqla)
                user = cur.fetchall()
    except Exception as e:
                print("error in executing with exception: ", e)
    return render_template('user/user.html',user=user,roles='')

#add unit
@app.route('/adduser',methods=['POST'])
def adduser():
    if not session.get("name"):
            return redirect("/login") 
    try:
        with getcursorSMLAO() as cur:   
                code = request.form['code']
                username = request.form['username']
                password = request.form['password']
                roles = request.form['roles']
                sql="INSERT INTO smlao_user(code, name_1,username, password, roles)VALUES (%s,%s,%s,%s,%s);"
                data=(code,username,username,password,roles)
                cur.execute(sql, data)
    except Exception as e:
            print("error in executing with exception: ", e)
    flash('ບັນທຶກສຳເລັດ')
    return redirect(url_for('user'))

#edit user
@app.route('/edituser/<string:id>',methods=['POST','GET'])
def edituser(id):
    if not session.get("name"):
            return redirect("/login") 
    try:
        with getcursorSMLAO() as cur:   
                id = request.form['code']
                username = request.form['username']
                password = request.form['password']
                roles = request.form['roles']

                sql="UPDATE smlao_user SET username = %s, password = %s, roles = %s WHERE code = %s"
                data=(username,password,roles, id)
                cur.execute(sql, data)
                flash('ແກ້ໄຂສຳເລັດ')
    except Exception as e:
            print("error in executing with exception: ", e)
    return redirect(url_for('user',id=id))

#delete user
@app.route('/delete_user/<string:id>',methods=['GET'])
def delete_user(id):
    if not session.get("name"):
            return redirect("/login") 
    try:
        with getcursorSMLAO() as cur:   
                user = session.get("name")
                sql="delete from smlao_user where code=%s AND username != %s"
                cur.execute(sql, (id,user))
                flash('ລົບສຳເລັດ')
    except Exception as e:
            print("error in executing with exception: ", e)
    return redirect(url_for('user'))