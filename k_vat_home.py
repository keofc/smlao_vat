from flask import Flask, render_template, request, redirect, url_for, session,make_response,jsonify, json,Response,flash
from app import app
from dbconn import *
import psycopg2 as p
from datetime import datetime, date
import pdfkit
import requests
import io
import xlwt

@app.route('/home')
def home():
    if not session.get("name"):
            return redirect("/login") 
    with getcursor(session.get("database")) as cur:
            if session.get("database") == 'sabaideevat':
                sort='asc'
            else:
                sort='desc'

            cur.execute("""SELECT row_number() OVER (order by doc_no """+sort+""") as rnum,doc_no, to_char(doc_date,'DD-MM-YYYY'),cust_code,b.name_1,
                            COALESCE(b.address,c.name_1,d.name_1),trim(to_char(round(total_amount),'999G999G999G999')),remark,vat_pass,approve_status FROM ic_trans a
                            left join ar_customer b on code=a.cust_code
                            left join province c on c.code=b.province
                            left join city d on d.province=b.province and d.code=b.city
                            where trans_flag=44 and to_char(doc_date,'MM')=to_char(current_date,'MM') 
                            order by doc_no """+sort+""" limit 10""")
            data = cur.fetchall()

            cur.execute("""
                                select count(doc_no),coalesce(sum(vat_pass),0),coalesce(sum(case when vat_pass=0 then 1 else 0 end),0) ,
                                coalesce(sum(round(total_amount)),0),coalesce(sum (round(total_vat_value)),0),
                                coalesce(sum(case when vat_pass=1 then round(total_amount) end),0),coalesce(sum(case when vat_pass=1 then round(total_vat_value) end),0),
                                coalesce(sum(case when vat_pass=0 then round(total_amount) end),0),coalesce(sum(case when vat_pass=0 then round(total_vat_value) end),0)
                                from ic_trans where to_char(doc_date,'MM')=to_char(current_date,'MM')""")
            head_all = cur.fetchone()
    return render_template('home/index.html',data=data,head_all=head_all,month_part=id,title=session.get("database"))


@app.route('/deleteDocno/<id>/<string:path>')
def deleteDocno(id,path):
        # print(check_doc(id))
        # if check_doc(id)==1:
        #        return '', 204
        # else:   
        with getcursor(session.get("database")) as cur:    

                cur.execute("select vat_pass from ic_trans where doc_no=%s",(id,))
                check_data = cur.fetchone()
                if check_data[0] == 1:
                        # return '',204
                        flash('ບໍ່ສາມາດລົບໄດ້!')
                        return redirect(url_for(path))
                else:   
                        cur.execute("delete from ic_trans where doc_no=%s AND vat_pass = 0",(id,))
                        cur.execute("delete from ic_trans_detail where doc_no=%s",(id,))
                        flash('ສຳເລັດ')
                        # return '',200
                        return redirect(url_for(path))
        # return redirect(url_for('Recipt'))

def check_doc(id):
    with getcursor(session.get("database")) as cur:
        cur.execute("select approve_status,vat_pass from ic_trans where doc_no=%s",(id,))
        check_data = cur.fetchone()
        candelete = 0
        if check_data[0]==1:
                candelete = 1
        if check_data[1]==1:
                candelete = 1
        return candelete
    


@app.route('/download/report/excel')
def download_report():
        style0 = xlwt.easyxf('font: name Times New Roman, color-index red, bold on',
        num_format_str='#,##0.00')
        style1 = xlwt.easyxf(num_format_str='D-MMM-YY')

        wb = xlwt.Workbook()
        ws = wb.add_sheet('A Test Sheet')

        ws.write(0, 0, 1234.56, style0)
        ws.write(1, 0, datetime.now(), style1)
        ws.write(2, 0, 1)
        ws.write(2, 1, 1)
        ws.write(2, 2, xlwt.Formula("A3+B3"))

        wb.save('example.xls')
        # workbook.save(output)
        # output.seek(0)
        # return Response(output, mimetype="application/ms-excel", headers={"Content-Disposition":"attachment;filename=employee_report.xls"})