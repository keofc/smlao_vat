CREATE TABLE IF NOT EXISTS smlao_user
(
    roworder serial PRIMARY KEY,
    code character varying COLLATE pg_catalog."default",
    name_1 character varying COLLATE pg_catalog."default",
    username character varying COLLATE pg_catalog."default",
    password character varying COLLATE pg_catalog."default",
    roles character varying COLLATE pg_catalog."default",
    status character varying COLLATE pg_catalog."default",
    create_date_time character varying COLLATE pg_catalog."default"
) 
CREATE TABLE IF NOT EXISTS smlao_databaes
(
    roworder serial PRIMARY KEY,
    code character varying COLLATE pg_catalog."default",
    name_1 character varying COLLATE pg_catalog."default",
    status character varying COLLATE pg_catalog."default",
    create_date_time character varying COLLATE pg_catalog."default"
) 


CREATE TABLE IF NOT EXISTS smlao_databaes_used
(
    roworder serial PRIMARY KEY,
    code character varying COLLATE pg_catalog."default",
    name_1 character varying COLLATE pg_catalog."default",
    user character varying COLLATE pg_catalog."default",
    status character varying COLLATE pg_catalog."default",
    create_date_time character varying COLLATE pg_catalog."default"
) 


CREATE TABLE IF NOT EXISTS smlao_vat_type
(
    roworder serial PRIMARY KEY,
    vat_status smallint
) 


select name_1,sn,p_model,p_brand,issue,emp_code,to_char(time_register,'MM-YYYY') as recipt_monthly,
interval_convert('minutes', time_finish_repair-time_register)::int,
case when interval_convert('minutes', time_finish_repair-time_register)<=4310 then 1 else '0' end as score_5,
case when interval_convert('minutes', time_finish_repair-time_register) between 4311 and 4741 then 1 else '0' end as score_4,
case when interval_convert('minutes', time_finish_repair-time_register) between 4742 and 5172 then 1 else '0' end as score_3,
case when interval_convert('minutes', time_finish_repair-time_register) between 5173 and 5603 then 1 else '0' end as score_2,
case when interval_convert('minutes', time_finish_repair-time_register) between 5604 and 6034 then 1 else '0' end as score_1,
case when interval_convert('minutes', time_finish_repair-time_register)> 6034  or time_finish_repair isnull and interval_convert('minutes', current_date-time_register)>6034  then 1 else '0' end as score_0,
case when time_finish_repair isnull and interval_convert('minutes', current_date-time_register)<6034  then 1 else 0 end as onpending,
case when interval_convert('minutes', time_finish_repair-time_register)<=4310  and time_finish_repair notnull then 5 
when interval_convert('minutes', time_finish_repair-time_register) between 4311 and 4741 and time_finish_repair notnull  then 4  
when interval_convert('minutes', time_finish_repair-time_register) between 4742 and 5172 and time_finish_repair notnull  then 3 
when interval_convert('minutes', time_finish_repair-time_register) between 5173 and 5603 and time_finish_repair notnull  then 2 
when interval_convert('minutes', time_finish_repair-time_register) between 5604 and 6034 and time_finish_repair notnull  then 1
when interval_convert('minutes', time_finish_repair-time_register)> 6034 and time_finish_repair notnull then 1 else '0' end as score
from tb_product where warrunty='ຮັບປະກັນ' and used_spare=0
UNION ALL
select name_1,sn,p_model,p_brand,issue,emp_code,to_char(time_register,'MM-YYYY') as recipt_monthly,
interval_convert('minutes', time_finish_repair-time_register),
case when interval_convert('minutes', time_finish_repair-time_register)<=5760 then 1 else '0' end as score_5,
case when interval_convert('minutes', time_finish_repair-time_register) between 5761 and 6336 then 1 else '0' end as score_4,
case when interval_convert('minutes', time_finish_repair-time_register) between 6336 and 6912 then 1 else '0' end as score_3,
case when interval_convert('minutes', time_finish_repair-time_register) between 6913 and 7488 then 1 else '0' end as score_2,
case when interval_convert('minutes', time_finish_repair-time_register) between 7489 and 8064 then 1 else '0' end as score_1,
case when interval_convert('minutes', time_finish_repair-time_register)> 8065  or time_finish_repair isnull and interval_convert('minutes', current_date-time_register)>8065 then 1 else '0' end as score_0,
case when time_finish_repair isnull and interval_convert('minutes', current_date-time_register)<8065  then 1 else 0 end as onpending,
case when interval_convert('minutes', time_finish_repair-time_register)<=5760  and time_finish_repair notnull then 5 
when interval_convert('minutes', time_finish_repair-time_register) between 5761 and 6336 and time_finish_repair notnull  then 4  
when interval_convert('minutes', time_finish_repair-time_register) between 6336 and 6912 and time_finish_repair notnull  then 3 
when interval_convert('minutes', time_finish_repair-time_register) between 6913 and 7488 and time_finish_repair notnull  then 2 
when interval_convert('minutes', time_finish_repair-time_register) between 7489 and 8064 and time_finish_repair notnull  then 1
when interval_convert('minutes', time_finish_repair-time_register)> 8065 and time_finish_repair notnull then 1 else '0' end as score
from tb_product where  warrunty='ໝົດຮັບປະກັນ' and used_spare=0
UNION ALL
select name_1,sn,p_model,p_brand,issue,emp_code,to_char(time_register,'MM-YYYY') as recipt_monthly,
interval_convert('minutes', time_finish_repair-time_register)::int,
case when interval_convert('minutes', time_finish_repair-time_register)<=4320 then 1 else '0' end as score_5,
case when interval_convert('minutes', time_finish_repair-time_register) between 4321 and 4752 then 1 else '0' end as score_4,
case when interval_convert('minutes', time_finish_repair-time_register) between 4753 and 5184 then 1 else '0' end as score_3,
case when interval_convert('minutes', time_finish_repair-time_register) between 5185 and 5616 then 1 else '0' end as score_2,
case when interval_convert('minutes', time_finish_repair-time_register) between 5617 and 6048 then 1 else '0' end as score_1,
case when interval_convert('minutes', time_finish_repair-time_register)> 6048  or time_finish_repair isnull and interval_convert('minutes', current_date-time_register)>6048  then 1 else '0' end as score_0,
case when time_finish_repair isnull and interval_convert('minutes', current_date-time_register)<6048  then 1 else 0 end as onpending,
case when interval_convert('minutes', time_finish_repair-time_register)<=4320  and time_finish_repair notnull then 5 
when interval_convert('minutes', time_finish_repair-time_register) between 4321 and 4752 and time_finish_repair notnull  then 4  
when interval_convert('minutes', time_finish_repair-time_register) between 4753 and 5184 and time_finish_repair notnull  then 3 
when interval_convert('minutes', time_finish_repair-time_register) between 5185 and 5616 and time_finish_repair notnull  then 2 
when interval_convert('minutes', time_finish_repair-time_register) between 5617 and 6048 and time_finish_repair notnull  then 1
when interval_convert('minutes', time_finish_repair-time_register)> 6048 and time_finish_repair notnull then 0 else '0' end as score
from tb_product where warrunty='ຮັບປະກັນ' and used_spare=1 and spare_order isnull
UNION ALL
select name_1,sn,p_model,p_brand,issue,emp_code,to_char(time_register,'MM-YYYY') as recipt_monthly,
interval_convert('minutes', time_finish_repair-time_register),
case when interval_convert('minutes', time_finish_repair-time_register)<=5760 then 1 else '0' end as score_5,
case when interval_convert('minutes', time_finish_repair-time_register) between 5761 and 6336 then 1 else '0' end as score_4,
case when interval_convert('minutes', time_finish_repair-time_register) between 6336 and 6912 then 1 else '0' end as score_3,
case when interval_convert('minutes', time_finish_repair-time_register) between 6913 and 7488 then 1 else '0' end as score_2,
case when interval_convert('minutes', time_finish_repair-time_register) between 7489 and 8064 then 1 else '0' end as score_1,
case when interval_convert('minutes', time_finish_repair-time_register)> 8065 or time_finish_repair isnull and interval_convert('minutes', current_date-time_register)>8065   then 1  else '0' end as score_0,
case when time_finish_repair isnull and interval_convert('minutes', current_date-time_register)<8065  then 1 else 0 end as onpending,
case when interval_convert('minutes', time_finish_repair-time_register)<=5760  and time_finish_repair notnull then 5 
when interval_convert('minutes', time_finish_repair-time_register) between 5761 and 6336 and time_finish_repair notnull  then 4  
when interval_convert('minutes', time_finish_repair-time_register) between 6336 and 6912 and time_finish_repair notnull  then 3 
when interval_convert('minutes', time_finish_repair-time_register) between 6913 and 7488 and time_finish_repair notnull  then 2 
when interval_convert('minutes', time_finish_repair-time_register) between 7489 and 8064 and time_finish_repair notnull  then 1
when interval_convert('minutes', time_finish_repair-time_register)> 8065 and time_finish_repair notnull then 1 else '0' end as score
from tb_product where  warrunty='ໝົດຮັບປະກັນ'  and used_spare=1 and spare_order isnull

UNION ALL
select name_1,sn,p_model,p_brand,issue,emp_code,to_char(time_register,'MM-YYYY') as recipt_monthly,
interval_convert('minutes', time_finish_repair-time_register)::int,
case when interval_convert('minutes', time_finish_repair-time_register)<=8640 then 1 else '0' end as score_5,
case when interval_convert('minutes', time_finish_repair-time_register) between 8641 and 9504 then 1 else '0' end as score_4,
case when interval_convert('minutes', time_finish_repair-time_register) between 9505 and 10368 then 1 else '0' end as score_3,
case when interval_convert('minutes', time_finish_repair-time_register) between 10369 and 11232 then 1 else '0' end as score_2,
case when interval_convert('minutes', time_finish_repair-time_register) between 11233 and 12096 then 1 else '0' end as score_1,
case when interval_convert('minutes', time_finish_repair-time_register)> 12096 or time_finish_repair isnull and interval_convert('minutes', current_date-time_register)>12096   then 1 else '0' end as score_0,
case when time_finish_repair isnull and interval_convert('minutes', current_date-time_register)<12096  then 1 else 0 end as onpending,
case when interval_convert('minutes', time_finish_repair-time_register)<=8640  and time_finish_repair notnull then 5 
when interval_convert('minutes', time_finish_repair-time_register) between 8641 and 9504 and time_finish_repair notnull  then 4  
when interval_convert('minutes', time_finish_repair-time_register) between 9505 and 10368 and time_finish_repair notnull  then 3 
when interval_convert('minutes', time_finish_repair-time_register) between 10369 and 11232 and time_finish_repair notnull  then 2 
when interval_convert('minutes', time_finish_repair-time_register) between 11233 and 12096 and time_finish_repair notnull  then 1
when interval_convert('minutes', time_finish_repair-time_register)> 12096 and time_finish_repair notnull then 1 else '0' end as score
from tb_product where warrunty='ຮັບປະກັນ' and used_spare=1 and spare_order NOTNULL
UNION ALL
select name_1,sn,p_model,p_brand,issue,emp_code,to_char(time_register,'MM-YYYY') as recipt_monthly,
interval_convert('minutes', time_finish_repair-time_register),
case when interval_convert('minutes', time_finish_repair-time_register)<=10080 then 1 else '0' end as score_5,
case when interval_convert('minutes', time_finish_repair-time_register) between 10081 and 11088 then 1 else '0' end as score_4,
case when interval_convert('minutes', time_finish_repair-time_register) between 11089 and 12096 then 1 else '0' end as score_3,
case when interval_convert('minutes', time_finish_repair-time_register) between 12097 and 13104 then 1 else '0' end as score_2,
case when interval_convert('minutes', time_finish_repair-time_register) between 13105 and 14112 then 1 else '0' end as score_1,
case when interval_convert('minutes', time_finish_repair-time_register)> 14112 or time_finish_repair isnull and interval_convert('minutes', current_date-time_register)>14112 then 1 else '0' end as score_0,
case when time_finish_repair isnull and interval_convert('minutes', current_date-time_register)<14112  then 1 else 0 end as onpending,
case when interval_convert('minutes', time_finish_repair-time_register)<=10080  and time_finish_repair notnull then 5 
when interval_convert('minutes', time_finish_repair-time_register) between 10081 and 11088 and time_finish_repair notnull  then 4  
when interval_convert('minutes', time_finish_repair-time_register) between 11089 and 12096 and time_finish_repair notnull  then 3 
when interval_convert('minutes', time_finish_repair-time_register) between 12097 and 13104 and time_finish_repair notnull  then 2 
when interval_convert('minutes', time_finish_repair-time_register) between 13105 and 14112 and time_finish_repair notnull  then 1
when interval_convert('minutes', time_finish_repair-time_register)> 14112 and time_finish_repair notnull then 1 else '0' end as score
from tb_product where  warrunty='ໝົດຮັບປະກັນ'  and used_spare=1 and spare_order NOTNULL