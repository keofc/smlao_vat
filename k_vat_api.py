from flask import Flask,request,make_response,jsonify,request
from flask_cors import CORS
import json
from dbconAPI import *
from dbconAPIMGT import *
import requests
from app import app


@app.route("/loginapp/<user>/<password>")
def loginapp(user,password):
  with getcursorAPMGT() as cur:
    cur.execute("select code,name_1,roles,database from smlao_user where username=%s and password=%s",(user,password,))
    users = cur.fetchone()
  return make_response(jsonify(users),200)

@app.route("/companyprofile/<database>")
def companyprofile(database):
  with getcursor(database) as cur:
      cur.execute("select code,name_1,name_2,address,address_2,tel,min,tin,vat_type from company_profile")
      companyprofile = cur.fetchone()
  return make_response(jsonify(companyprofile),200)

@app.route("/homedash/<database>")
def homedash(database):
  with getcursor(database) as cur:
    cur.execute("""select coalesce(  trim(to_char(sum(value_before_vat),'999G999G999G999')),'0') as value_before_vat,
    coalesce( trim(to_char(sum (total_vat_value),'999G999G999G999')),'0') as total_vat_value
    ,coalesce(trim(to_char(sum (total_amount),'999G999G999G999')),'0') as total_amount
                    from ic_trans where to_char(doc_date,'MM')=to_char(current_date,'MM') and vat_pass=1""")
    data = cur.fetchone()
  return make_response(jsonify(data),200)


@app.route("/monthrecipt/<database>")
def saletotalday(database):
  with getcursor(database) as cur:
    sql="""select to_char(doc_date,'YYYY-MM') as dd,sum(total_amount)::text as balance_amount from ic_trans where vat_pass=1 
group by to_char(doc_date,'YYYY-MM') order by to_char(doc_date,'YYYY-MM') DESC limit 5"""
    cur.execute(sql)
    result = cur.fetchall()
  return make_response(jsonify(result),200) 
    

@app.route("/saleday/<database>")
def saleday(database):
  with getcursor(database) as cur:
    sql="""select doc_no,b.name_1 as ar_name,trim(to_char(total_amount,'999G999G999G999D99')) amount,vat_pass
            from ic_trans a
            left join ar_customer b on b.code=a.cust_code
            order by doc_no DESC"""
    cur.execute(sql)
    result = cur.fetchall()
  return make_response(jsonify(result),200) 

@app.route("/salemonthlist/<database>/<month>")
def salemonthlist(database,month):
  with getcursor(database) as cur:
    sql="""select doc_no,to_char(doc_date,'dd-MM-yyyy') as doc_date,b.name_1 as ar_name,trim(to_char(value_before_vat,'999G999G999G999D99')) as value_before_vat,trim(to_char(total_vat_value,'999G999G999G999D99')) as total_vat_value,trim(to_char(total_amount,'999G999G999G999D99')) as amount,
    vat_pass,(select count(item_code) from ic_trans_detail where doc_no=a.doc_no) as count_iteme
            from ic_trans a
            left join ar_customer b on b.code=a.cust_code
            where to_char(doc_date,'MM-YYYY')=%s
            order by doc_no DESC"""
    cur.execute(sql,(month,))
    result = cur.fetchall()
  return make_response(jsonify(result),200) 

@app.route("/monthpart/<database>/<id>")
def monthpart(database,id):
  with getcursor(database) as cur:
    cur.execute("""select coalesce(trim(to_char(sum(value_before_vat),'999G999G999G999D99')),'0.00') as value_before_vat
                    ,coalesce(trim(to_char(sum(total_vat_value),'999G999G999G999D99')),'0.00') as total_vat_value,
                    coalesce(trim(to_char(sum(total_amount),'999G999G999G999D99')),'0.00') as total_amount
                    from ic_trans a
                    where to_char(doc_date,'MM-YYYY')=%s
""",(id,))
    data = cur.fetchone()
  return make_response(jsonify(data),200)



@app.route("/vatpass/<database>")
def vatpass(database):
  with getcursor(database) as cur:
    sql="""select doc_no,to_char(doc_date,'dd-MM-yyyy') as doc_date,b.name_1 as ar_name,trim(to_char(value_before_vat,'999G999G999G999D99')) as value_before_vat,trim(to_char(total_vat_value,'999G999G999G999D99')) as total_vat_value,trim(to_char(total_amount,'999G999G999G999D99')) as amount,
    vat_pass,(select count(item_code) from ic_trans_detail where doc_no=a.doc_no) as count_iteme
            from ic_trans a
            left join ar_customer b on b.code=a.cust_code
            where vat_pass=0
            order by doc_no DESC"""
    cur.execute(sql)
    result = cur.fetchall()
  return make_response(jsonify(result),200) 


@app.route('/postvatbyapp/<database>/<doc_no>')
def postvatbyapp(database,doc_no):
    link = "http://150.95.90.124:4433/vatpost/"+database+"/"+doc_no
    f = requests.get(link)
    return make_response(jsonify({"message":f.text}), 200 )