from flask import Flask,render_template

app = Flask(__name__)

app.config["SESSION_PERMANENT"] = False
app.config["SESSION_TYPE"] = "filesystem"
app.secret_key = "secret key"

from  login import *
from  k_vat_home import *
from  k_vat_manage import *
from  k_vat_recipt import *
from  k_vat_contract import *
from  k_vat_jsonsss import *
from  k_vat_product import *
from  k_vat_post import *
from  k_vat_sale_report import *
from  k_vat_user import *
from  k_vat_company import *
# mobile App
from  k_vat_api import *


if __name__ == "__main__":
    app.run(debug=True,port='80',host='0.0.0.0')
    
    