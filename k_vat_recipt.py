from flask import Flask, render_template, request, redirect, url_for, session,make_response,jsonify
from app import app
from dbconn import *
import psycopg2 as p
from datetime import datetime, date
import pdfkit
import requests
from k_vat_post import *

# @app.route('/home')
# def home():
#     link = "http://150.95.90.124:4433/emppost"
#     f = requests.get(link)
#     print(f.text)
#     return render_template('home/home.html')


@app.route('/Recipt/<id>')
@app.route('/Recipt', defaults={'id':''})
def Recipt(id):
    if not session.get("name"):
            return redirect("/login") 
    print(session.get("name"))
    with getcursor(session.get("database")) as cur:
            dateTimeObj = datetime.now()
            timestampStr = dateTimeObj.strftime("%Y-%m")
            
            if id=='':
                id=timestampStr

            if session.get("database") == 'sabaideevat':
                sort='asc'
            else:
                sort='desc'

            cur.execute("""SELECT row_number() OVER (order by doc_no """+sort+""") as rnum,doc_no, to_char(doc_date,'DD-MM-YYYY'),cust_code,b.name_1,
                                COALESCE(b.address,c.name_1,d.name_1),trim(to_char(total_amount,'999G999G999G999D99')),remark,vat_pass,approve_status FROM ic_trans a
                                left join ar_customer b on code=a.cust_code
                                left join province c on c.code=b.province
                                left join city d on d.province=b.province and d.code=b.city
                                where trans_flag=44 and to_char(doc_date,'YYYY-MM')=%s
                                order by doc_no """+sort,(id,))
            polist = cur.fetchall()
    return render_template('Recipt/index.html',polist=polist,month_part=id,title=session.get("database"))



@app.route('/VATPASS/<id>')
@app.route('/VATPASS', defaults={'id':''})
def VATPASS(id):
    if not session.get("name"):
            return redirect("/login") 
    with getcursor(session.get("database")) as cur:
            dateTimeObj = datetime.now()
            timestampStr = dateTimeObj.strftime("%Y-%m")
            
            if id=='':
                id=timestampStr

            if session.get("database") == 'sabaideevat':
                sort='asc'
            else:
                sort='desc'

            cur.execute("""SELECT row_number() OVER (order by doc_no """+sort+""") as rnum,doc_no, to_char(doc_date,'DD-MM-YYYY'),cust_code,b.name_1,
                                COALESCE(b.address,c.name_1,d.name_1),trim(to_char(total_amount,'999G999G999G999D99')),remark,vat_pass,approve_status FROM ic_trans a
                                left join ar_customer b on code=a.cust_code
                                left join province c on c.code=b.province
                                left join city d on d.province=b.province and d.code=b.city
                                where trans_flag=44 and to_char(doc_date,'YYYY-MM')=%s and vat_pass=1
                                order by doc_no """+sort,(id,))
            polist = cur.fetchall()
    return render_template('Recipt/vatpass.html',polist=polist,month_part=id,title=session.get("database"))


@app.route('/VATNO/<id>')
@app.route('/VATNO', defaults={'id':''})
def VATNO(id):
    if not session.get("name"):
            return redirect("/login") 
    with getcursor(session.get("database")) as cur:
            dateTimeObj = datetime.now()
            timestampStr = dateTimeObj.strftime("%Y-%m")
            
            if id=='':
                id=timestampStr

            if session.get("database") == 'sabaideevat':
                sort='asc'
            else:
                sort='desc'
         
            cur.execute("""SELECT row_number() OVER (order by doc_no """+sort+""") as rnum,doc_no, to_char(doc_date,'DD-MM-YYYY'),cust_code,b.name_1,
                                COALESCE(b.address,c.name_1,d.name_1),trim(to_char(total_amount,'999G999G999G999D99')),remark,vat_pass,approve_status FROM ic_trans a
                                left join ar_customer b on code=a.cust_code
                                left join province c on c.code=b.province
                                left join city d on d.province=b.province and d.code=b.city
                                where trans_flag=44 and to_char(doc_date,'YYYY-MM')=%s and vat_pass=0
                                order by doc_no """+sort,(id,))
            polist = cur.fetchall()
    return render_template('Recipt/vatno.html',polist=polist,month_part=id,title=session.get("database"))

@app.route('/ReciptNew/<id>/<id1>')
@app.route('/ReciptNew', defaults={'id':'00','id1':'1'})
def ReciptNew(id,id1):
    if not session.get("name"):
            return redirect("/login") 
    with getcursor(session.get("database")) as cur:
            # cur = condb("sabaideevat").cursor()
            dateTimeObj = datetime.now()
            timestampStr = dateTimeObj.strftime("%Y%m")
            timestampStr2 = dateTimeObj.strftime("%Y-%m")
            a = str(timestampStr)
            a2 = str(timestampStr2)
            sql_d = "select max(SUBSTRING (doc_no, 4)::bigint)  from ic_trans  where trans_flag='44' and to_char(doc_date,'yyyy-mm')=%s"
            cur.execute(sql_d,(a2,))
            bil_no = cur.fetchone()
            if bil_no[0] == None:
                doc_no = 'INV'+a+'0001'
            else:
                doc = bil_no[0]
                doc_no = 'INV' + str(1 + int(doc))
            day_ = dateTimeObj.strftime("%Y-%m-%d")

            cur.execute("SELECT code, name_1,tel  FROM ar_customer")
            ar_customer = cur.fetchall()
            sql=""" SELECT a.code, a.name_1, address||', ເມືອງ '||c.name_1||', ເເຂວງ '||b.name_1, tel,tin
                    FROM ar_customer a
                    left join public.province b on b.code=a.province
                    left join public.city c on c.province=a.province and c.code=a.city
                    where a.code=%s
                        """
            cur.execute(sql,(id,))
            customer = cur.fetchone()
            sqlpr="select  row_number() OVER () as rnum,code,name_1,unit_code,case when tax_type = 0 then 'ອມພ 0' when tax_type = 1 then 'ອມພແຍກນອກ' when tax_type = 2 then 'ອມພລວມໃນ' when tax_type = 3 then 'ຍົກເວັ້ນ ອມພ' else '' end as tax_type,tax_type from ic_inventory"
            cur.execute(sqlpr)
            product = cur.fetchall()
            print('dffds', product)
            podetail = ic_trans_detail(session.get("name"))
            total_value=0
            total_amount=0
            total_vat_values=0
            total_befor_vat=0
            for item in podetail:
                total_value +=item[5]
                total_vat_values +=item[8]
                total_amount +=item[9]
            if id1=='2':
                total_befor_vat=total_amount-total_vat_values
            else:
                total_befor_vat=total_value 
    return render_template('Recipt/rcnew.html',
                            doc_no=doc_no,
                            saletype=id,
                            vat_type_default=id1,
                            day_=day_,
                            customer=customer,
                            ar_customer=ar_customer,
                            user='',
                            tt_bill='',
                            podetail =podetail,
                            product=product,
                            total_value=total_value,
                            total_amount=total_amount,
                            total_vat_values=total_vat_values,
                            total_befor_vat=total_befor_vat,
                            title=session.get("database")
                            ,username=session.get("name")
                            )
                            

@app.route('/vatprocess/<id>/<id1>')
def vatprocess(id,id1):
    if not session.get("name"):
            return redirect("/login") 
    try:
        if id1 == '0':
            conn.execute("update ic_trans_detail set vat_value=0, total_amount=sum_amount where trans_flag=?", (44,))
        elif  id1 == '1':
            conn.execute("update ic_trans_detail set vat_value=sum_amount*(0.07), total_amount=sum_amount+(sum_amount*(0.07)) where trans_flag=? AND tax_type !=0", (44,))
        else:
            conn.execute("update ic_trans_detail set vat_value=(sum_amount/(0.93))-sum_amount,total_amount=sum_amount where trans_flag=? AND tax_type !=0", (44,))
        conn.commit()
    except Exception as e:
                print("error in executing with exception: ", e)
    return redirect(url_for('ReciptNew',id=id,id1=id1))

@app.route('/addpoitem',methods=['POST'])
def addpoitem():
    if not session.get("name"):
            return redirect("/login") 
    try:
        cust_code=request.form['custcode']
        if cust_code=='':
            cust_code ='00'
        item_code=request.form['code']
        item_name=request.form['name_1']
        unit_code=request.form['unit_code']
        vate_defualt=request.form['vate_defualt']
        tax_type=request.form['tax_type']
        data=(cust_code,item_code,item_name,unit_code,1,0,0,0,7,0,0,44,session.get("name"),int(tax_type))
        conn.execute("""INSERT INTO ic_trans_detail(cust_code, iteme_code, iteme_name, unit_code, qty, discount, price, sum_amount, vat_rate, vat_value, total_amount,trans_flag,user_created,tax_type)VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)""",data)
        conn.commit()
        print("เพิ่มระเบียงข้อมูลสำเร็จ")
    except Exception as e:
                print("error in executing with exception: ", e)
    return redirect(url_for('ReciptNew',id=cust_code,id1=vate_defualt))


@app.route('/updatepoqty',methods=['POST'])
def updatepoqty():
    if not session.get("name"):
            return redirect("/login") 
    try:
        vate_defualt=request.form['vate_defualt']
        cust_code=request.form['cust_code']
        roworder=request.form['roworder']
        qty=request.form['qty'].replace(',', '')
        price=request.form['price'].replace(',', '')
        if vate_defualt=='0':
            vat_rat=0
            total_amount=float(price)*float(qty)
        elif vate_defualt=='1':
            vat_rat=(float(price)*float(qty))*(0.07)
            total_amount=float(price)*float(qty)+vat_rat
        else:
            vat_rat=float(price)*float(qty)-float(price)*float(qty)*(0.93)
            total_amount=float(price)*float(qty)
        conn.execute("update ic_trans_detail set qty=?,sum_amount=price*?, vat_value=?, total_amount=? where roworder=? AND tax_type !=0", (qty,qty,vat_rat,total_amount,roworder,))
        conn.commit()

        conn.execute("update ic_trans_detail set qty=?,sum_amount=price*?, vat_value=0, total_amount=price*? where roworder=? AND tax_type =0", (qty,qty,qty,roworder,))
        conn.commit()
    except Exception as e:
                print("error in executing with exception: ", e)
    return redirect(url_for('ReciptNew',id=cust_code,id1=vate_defualt))

@app.route('/updatepoprice',methods=['POST'])
def updatepoprice():
    if not session.get("name"):
            return redirect("/login") 
    try:
        vate_defualt=request.form['vate_defualt']
        cust_code=request.form['cust_code']
        roworder=request.form['roworder']
        price=request.form['price'].replace(',', '')
        qty=request.form['qty'].replace(',', '')
        if vate_defualt=='0':
            vat_rat=0
            total_amount=float(price)*float(qty)
        elif vate_defualt=='1':
            vat_rat=(float(price)*float(qty))*(0.07)
            total_amount=float(price)*float(qty)+vat_rat
        else:
            vat_rat=float(price)*float(qty)-float(price)*float(qty)*(0.93)
            total_amount=float(price)*float(qty)

        conn.execute("update ic_trans_detail set price=?,sum_amount=qty*?, vat_value=?, total_amount=? where roworder=? AND tax_type !=0", (price,price,vat_rat,total_amount,roworder,))
        conn.commit()

        conn.execute("update ic_trans_detail set price=?,sum_amount=qty*?, vat_value=0, total_amount=qty*? where roworder=? AND tax_type =0", (price,price,price,roworder,))
        conn.commit()
    except Exception as e:
                print("error in executing with exception: ", e)
    return redirect(url_for('ReciptNew',id=cust_code,id1=vate_defualt))

@app.route('/updatename',methods=['POST'])
def updatename():
    if not session.get("name"):
            return redirect("/login") 
    try:
        vate_defualt=request.form['vate_defualt']
        cust_code=request.form['cust_code']
        roworder=request.form['roworder']
        name_1=request.form['name_1']
        conn.execute("update ic_trans_detail set iteme_name=? where roworder=?", (name_1,roworder,))
        conn.commit()
    except Exception as e:
                print("error in executing with exception: ", e)
    return redirect(url_for('ReciptNew',id=cust_code,id1=vate_defualt))
@app.route('/deletepoitem/<id>/<id1>/<id2>')
def deletepoitem(id,id1,id2):
    if not session.get("name"):
            return redirect("/login") 
    try:
        conn.execute("delete from ic_trans_detail where roworder=?", (id,))
        conn.commit()
    except Exception as e:
                print("error in executing with exception: ", e)
    return redirect(url_for('ReciptNew',id=id1,id1=id2))


@app.route('/saverc',methods=['POST'])
def saverc():
    # try:    
    with getcursor(session.get("database")) as cur:  
        sql_ic_trans_detail="""
        INSERT INTO ic_trans_detail(trans_flag, trans_type, doc_no, doc_date, cust_code, item_code, item_name, unit_code, qty, price,discount,sum_amount,vate_rate, total_value, total_vat_value, total_amount) VALUES
              (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s);
            """
        dateTimeObj = datetime.now()
        doc_time = dateTimeObj.strftime("%H:%M")
        saletype=request.form['saletypesss']
        vattype_value=request.form['vattype_value']
            # cust=request.form['cust']
        doc_no=request.form['doc_no']
        doc_date=request.form['doc_date']
        op_tramark=request.form['op_tramark']
        podetail = ic_trans_detail(session.get("name"))
        total_value=0
        total_amount=0
        total_vat_values=0
        total_befor_vat=0
        for item in podetail:
            total_value +=item[5]
            total_vat_values +=item[8]
            total_amount +=item[9]
            cur.execute(sql_ic_trans_detail,(44,0,doc_no,doc_date,saletype,item[1],item[2],item[4],item[3],item[5],item[6],item[7],7,item[7],item[8],item[9]))
        if saletype=='2':
            total_befor_vat=total_amount-total_vat_values
        else:
            total_befor_vat=total_value
        sql_ic_trans="""
                            INSERT INTO ic_trans(trans_flag, trans_type, doc_no, doc_date,doc_time, cust_code, vat_rat, total_value, discount_amount, value_before_vat, total_vat_value, total_amount, remark,vat_type)
                            VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"""
        datahe=(44,0,doc_no,doc_date,doc_time,saletype,7,total_value,0,total_befor_vat,total_vat_values,total_amount,op_tramark,vattype_value)
        cur.execute(sql_ic_trans,datahe)
        if checkvatpass=='1':
              postvat(doc_no)
        conn.execute("delete from ic_trans_detail")
        conn.commit()
    return redirect(url_for('Recipt'))
        

@app.route("/printbill/<id>")
def printbill(id):
    with getcursor(session.get("database")) as cur:   
            sql="""SELECT name_1,name_2, address, tel, min, tin,vat_license, to_char(date_vat_license,'DD-MM-YYYY'),COALESCE(bank_name,'') ,COALESCE(bank_number,'')   FROM company_profile"""
            cur.execute(sql,(id,))
            company = cur.fetchone()
            cust_code=session.get('cust_code')
            sql_b="""SELECT doc_no, to_char(doc_date,'DD-MM-YYYY'),cust_code,b.name_1,COALESCE(address,''),COALESCE(d.name_1,''),COALESCE(c.name_1,''),COALESCE(b.tel,''),tin,
            trim( to_char(round(total_amount-total_vat_value),'999G999G999G999') ),
            trim( to_char(round(total_vat_value),'999G999G999G999') ),
            trim( to_char(round(total_amount),'999G999G999G999') ),
            (select fx_numtowords from fx_numtowords(total_amount::bigint))||'ກີບຖ້ວນ' as words_count,a.remark FROM ic_trans a
            left join ar_customer  b on code=a.cust_code
            left join province c on c.code=b.province
            left join city d on d.province=b.province and d.code=b.city
            where doc_no=%s"""
            cur.execute(sql_b,(id,))
            customer = cur.fetchone()
            sql_dt="""select row_number() OVER () as rnum,item_name,round(qty,0),unit_code,
            price,sum_amount,total_vat_value,total_amount,
					trim( to_char(round(price),'999G999G999G999') ),
					trim( to_char(round(sum_amount),'999G999G999G999') ),
					trim( to_char(round(total_vat_value),'999G999G999G999') ),
					trim( to_char(round(total_amount),'999G999G999G999') ) 
					from ic_trans_detail where doc_no=%s order by roworder"""
            cur.execute(sql_dt,(id,))
            bill_dt = cur.fetchall()
            count_dt = len(bill_dt)
            print(count_dt)
            total_value=0
            total_amount=0
            total_vat_values=0
            total_befor_vat=0
            for item in bill_dt:
                total_value +=item[5]
                total_vat_values +=item[6]
                total_amount +=item[7]
            html = render_template("print/tax_invoice.html",company=company,customer=customer,bill_dt=bill_dt,total_value=total_value,total_vat_values=total_vat_values,total_amount=total_amount,count_dt=count_dt)
            options = {
                        'page-size': 'A4',
                        'margin-top': '0in',
                        'margin-right': '0in',
                        'margin-bottom': '0in',
                        'margin-left': '0in',
                        'encoding': "UTF-8",
                        'no-outline': None,
                        'dpi': 300,
                        "disable-smart-shrinking": True
                        }
            config = pdfkit.configuration(wkhtmltopdf="C:\\Program Files\\wkhtmltopdf\\bin\\wkhtmltopdf.exe")
            pdf = pdfkit.from_string(html, False,options=options,configuration=config)
            # pdf = pdfkit.from_string(html, False,options=options)
            response = make_response(pdf)

            response.headers["Content-Type"] = "application/pdf"
            response.headers["Content-Disposition"] = "inline; filename=tax-invoice.pdf"
            return response
    

def checkvatpass():
    with getcursor(session.get("database")) as cur:
        cur.execute(" select * from smlao_vat_type")
        data = cur.fetchone()
    return data[1]



@app.route('/cleaivdraft')
def cleaivdraft():
    if not session.get("name"):
            return redirect("/login") 
    try:
        conn.execute("delete from ic_trans_detail where user_created=?", (session.get("name"),))
        conn.commit()
    except Exception as e:
                print("error in executing with exception: ", e)
    return redirect(url_for('home'))


@app.route("/change_doc/<string:ym>/<string:cm>",methods=['GET'])
def change_doc(ym,cm):
    with getcursor(session.get("database")) as cur:
        sql_d = "select max(SUBSTRING (doc_no, 4)::bigint)  from ic_trans  where trans_flag='44' and to_char(doc_date,'yyyy-mm')=%s"
        cur.execute(sql_d,(cm,))
        bil_no = cur.fetchone()

        if bil_no[0] == None:
            doc_no = 'INV'+ym+'0001'
        else:
            doc = bil_no[0]
            doc_no = 'INV' +str(1 + int(doc))

        cityss=[(doc_no),]
        print('POOO',cityss,doc_no)

    return jsonify({'citylist': cityss})