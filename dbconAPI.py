from flask import Flask, render_template, request, redirect, url_for, session,make_response
from app import app
from psycopg2.pool import SimpleConnectionPool
from contextlib import contextmanager
from psycopg2.extras import RealDictCursor
# dbConnection = "dbname='sabaideevat' user='postgres' host='150.95.90.124' password='sml'"

#     # pool define with 10 live connections
# connectionpool = SimpleConnectionPool(1,10,dsn=dbConnection)

@contextmanager
def getcursor(id):
    dbConnection = "dbname='"+id+"' user='postgres' host='150.95.90.124' password='sml'"
    # pool define with 10 live connections
    connectionpool = SimpleConnectionPool(1,10,dsn=dbConnection)
    con = connectionpool.getconn()
    con.autocommit = True
    try:
        yield con.cursor(cursor_factory=RealDictCursor)
    finally:
        connectionpool.putconn(con)