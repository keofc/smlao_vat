from flask import Flask, render_template, request, redirect, url_for, session,make_response
from app import app
from dbconn import *
import psycopg2 as p
from datetime import datetime, date
import pdfkit
import requests



@app.route('/vat_trans_type')
def vat_trans_type():
    if not session.get("name"):
            return redirect("/login") 
    with getcursor(session.get("database")) as cur:
        cur.execute(" select * from smlao_vat_type")
        data = cur.fetchone()
        print(data[1])
    return render_template('Vat/index.html',data=data[1])



@app.route('/postvat/<id>')
def postvat(id):


    link = "http://150.95.90.124:4433/vatpost/"+session.get("database")+"/"+id
    f = requests.get(link)
    print(f.text)
    if f.text=='success':
        return redirect(url_for('home'))
    else:
        return "ERROR"
    
