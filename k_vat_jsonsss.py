from flask import Flask, render_template, request, redirect, url_for, session, jsonify, json
from dbconn import *
from app import app



@app.route("/sel_city/<id>")
def sel_city(id):
    try:
        with getcursor(session.get("database")) as cur:
                sql="SELECT code,name_1 FROM city where province=%s ORDER BY roworder ASC "
                cur.execute(sql,(id,))
                city = cur.fetchall()
                print( city)
    except Exception as e:
                print("error in executing with exception: ", e)
    return jsonify({'city': city})