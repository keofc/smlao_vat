from flask import Flask, render_template, request, redirect, url_for, session,make_response
from app import app
from dbconn import *
import psycopg2 as p
from datetime import datetime, date
import pdfkit
import requests


#view company
@app.route('/company_profile/<id>')
@app.route('/company_profile', defaults={'id':'ແກ້ໃຂ'})
def company_profile(id):
    if not session.get("name"):
            return redirect("/login") 
    try:
        with getcursor(session.get("database")) as cur:
            sql="select code,name_1,name_2,address,address_2,tel,min,tin from company_profile"
            cur.execute(sql)
            company = cur.fetchone()
            # sqlch="SELECT count(roworder) FROM company_profile"
            # cur.execute(sqlch)
            # count_company = cur.fetchall()
            btn_label=id
    except Exception as e:
        print("error in executing with exception: ", e)
    return render_template('CompanyProfile/company.html',company=company, btn_label=btn_label)
    # return render_template('CompanyProfile/company.html',data,title=session.get("database"))


@app.route('/updatecompany',methods=['POST'])
def updatecompany():
    if not session.get("name"):
            return redirect("/login") 


    btn_labe=request.form['btn_label']
    if btn_labe=='ແກ້ໃຂ':
        return redirect(url_for('company_profile',id='ບັກທຶກ'))
    else:
        try:
            with getcursor(session.get("database")) as cur:
                code=request.form['code']
                name_1=request.form['name_1']
                name_2=request.form['name_2']
                address_1=request.form['address_1']
                address_2=request.form['address_2']
                tel=request.form['tel']
                min=request.form['min']
                tin=request.form['tin']
                cur.execute("update company_profile set name_1=%s,name_2=%s,address=%s,address_2=%s,tel=%s,min=%s,tin=%s where code=%s",(name_1,name_2,address_1,address_2,tel,min,tin,code,))
        except Exception as e:
                    print("error in executing with exception: ", e)
        return redirect(url_for('company_profile'))
