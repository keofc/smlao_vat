
from flask import Flask, render_template, request, redirect, url_for, session,make_response
from app import app
from dbconn import *
import psycopg2 as p
from datetime import datetime, date,timezone
import pdfkit
import requests

@app.route('/saledaiyreport', methods=["POST", "GET"])
def saledaiyreport():
    dateTimeObj = datetime.now()
    day_ = dateTimeObj.strftime("%Y-%m-%d")
    with getcursor(session.get("database")) as cur:
        if request.method == "POST":
            from_date = request.form['from_date']
            to_date= request.form['to_date']
            cur.execute("""SELECT row_number() OVER (order by doc_no) as rnum,doc_no, to_char(doc_date,'DD-MM-YYYY'),b.name_1,
                            COALESCE(b.address,c.name_1,d.name_1),remark,trim(to_char(value_before_vat,'999G999G999G999D99')) as value_before_vat,
                            trim(to_char(total_vat_value,'999G999G999G999D99')) as total_vat_value,
                            trim(to_char(total_amount,'999G999G999G999D99')) as total_amount
                            FROM ic_trans a
                            left join ar_customer b on code=a.cust_code
                            left join province c on c.code=b.province
                            left join city d on d.province=b.province and d.code=b.city
                            where trans_flag=44 and doc_date between %s and %s
                            order by doc_no""",(from_date,to_date,))
            data = cur.fetchall()
        else:
            from_date = day_
            to_date=day_
            cur.execute("""SELECT row_number() OVER (order by doc_no) as rnum,doc_no, to_char(doc_date,'DD-MM-YYYY'),b.name_1,
                            COALESCE(b.address,c.name_1,d.name_1),remark,trim(to_char(value_before_vat,'999G999G999G999D99')) as value_before_vat,
                            trim(to_char(total_vat_value,'999G999G999G999D99')) as total_vat_value,
                            trim(to_char(total_amount,'999G999G999G999D99')) as total_amount
                            FROM ic_trans a
                            left join ar_customer b on code=a.cust_code
                            left join province c on c.code=b.province
                            left join city d on d.province=b.province and d.code=b.city
                            where trans_flag=44 and doc_date between %s and %s
                            order by doc_no """,(from_date,to_date,))
            data = cur.fetchall()
        countdata = len(data)
        total_befor_vat=0
        total_vat = 0
        total_amoun=0

        for items in data:
            total_befor_vat += float(items[6].replace(',', ''))
            total_vat +=  float(items[7].replace(',', ''))
            total_amoun +=  float(items[8].replace(',', ''))


    return render_template('Report/daiy_recipt.html',
                           from_date=from_date,
                           to_date=to_date,
                           data=data,
                           title=session.get("database"),
                           countdata=countdata,
                           total_befor_vat=total_befor_vat,
                           total_vat=total_vat,
                           total_amoun=total_amoun
                           )

@app.route('/vatpassreport', methods=["POST", "GET"])
def vatpassreport():
    dateTimeObj = datetime.now()
    day_ = dateTimeObj.strftime("%Y-%m-%d")
    with getcursor(session.get("database")) as cur:
        if request.method == "POST":
            from_date = request.form['from_date']
            to_date= request.form['to_date']
            cur.execute("""SELECT row_number() OVER (order by doc_no) as rnum,doc_no, to_char(doc_date,'DD-MM-YYYY'),b.name_1,
                            COALESCE(b.address,c.name_1,d.name_1),remark,trim(to_char(value_before_vat,'999G999G999G999D99')) as value_before_vat,
                            trim(to_char(total_vat_value,'999G999G999G999D99')) as total_vat_value,
                            trim(to_char(total_amount,'999G999G999G999D99')) as total_amount
                            FROM ic_trans a
                            left join ar_customer b on code=a.cust_code
                            left join province c on c.code=b.province
                            left join city d on d.province=b.province and d.code=b.city
                            where trans_flag=44 and doc_date between %s and %s
                            order by doc_no""",(from_date,to_date,))
            data = cur.fetchall()
        else:
            from_date = day_
            to_date=day_
            cur.execute("""SELECT row_number() OVER (order by doc_no) as rnum,doc_no, to_char(doc_date,'DD-MM-YYYY'),b.name_1,
                            COALESCE(b.address,c.name_1,d.name_1),remark,trim(to_char(value_before_vat,'999G999G999G999D99')) as value_before_vat,
                            trim(to_char(total_vat_value,'999G999G999G999D99')) as total_vat_value,
                            trim(to_char(total_amount,'999G999G999G999D99')) as total_amount
                            FROM ic_trans a
                            left join ar_customer b on code=a.cust_code
                            left join province c on c.code=b.province
                            left join city d on d.province=b.province and d.code=b.city
                            where trans_flag=44 and doc_date between %s and %s and vat_pass=1
                            order by doc_no """,(from_date,to_date,))
            data = cur.fetchall()
        countdata = len(data)
        total_befor_vat=0
        total_vat = 0
        total_amoun=0

        for items in data:
            total_befor_vat += float(items[6].replace(',', ''))
            total_vat +=  float(items[7].replace(',', ''))
            total_amoun +=  float(items[8].replace(',', ''))


    return render_template('Report/vatpassreport.html',
                           from_date=from_date,
                           to_date=to_date,
                           data=data,
                           title=session.get("database"),
                           countdata=countdata,
                           total_befor_vat=total_befor_vat,
                           total_vat=total_vat,
                           total_amoun=total_amoun
                           )


@app.route('/vatnoreport', methods=["POST", "GET"])
def vatnoreport():
    dateTimeObj = datetime.now()
    day_ = dateTimeObj.strftime("%Y-%m-%d")
    with getcursor(session.get("database")) as cur:
        if request.method == "POST":
            from_date = request.form['from_date']
            to_date= request.form['to_date']
            cur.execute("""SELECT row_number() OVER (order by doc_no) as rnum,doc_no, to_char(doc_date,'DD-MM-YYYY'),b.name_1,
                            COALESCE(b.address,c.name_1,d.name_1),remark,trim(to_char(value_before_vat,'999G999G999G999D99')) as value_before_vat,
                            trim(to_char(total_vat_value,'999G999G999G999D99')) as total_vat_value,
                            trim(to_char(total_amount,'999G999G999G999D99')) as total_amount
                            FROM ic_trans a
                            left join ar_customer b on code=a.cust_code
                            left join province c on c.code=b.province
                            left join city d on d.province=b.province and d.code=b.city
                            where trans_flag=44 and doc_date between %s and %s
                            order by doc_no""",(from_date,to_date,))
            data = cur.fetchall()
        else:
            from_date = day_
            to_date=day_
            cur.execute("""SELECT row_number() OVER (order by doc_no) as rnum,doc_no, to_char(doc_date,'DD-MM-YYYY'),b.name_1,
                            COALESCE(b.address,c.name_1,d.name_1),remark,trim(to_char(value_before_vat,'999G999G999G999D99')) as value_before_vat,
                            trim(to_char(total_vat_value,'999G999G999G999D99')) as total_vat_value,
                            trim(to_char(total_amount,'999G999G999G999D99')) as total_amount
                            FROM ic_trans a
                            left join ar_customer b on code=a.cust_code
                            left join province c on c.code=b.province
                            left join city d on d.province=b.province and d.code=b.city
                            where trans_flag=44 and doc_date between %s and %s
                            order by doc_no """,(from_date,to_date,))
            data = cur.fetchall()
        countdata = len(data)
        total_befor_vat=0
        total_vat = 0
        total_amoun=0

        for items in data:
            total_befor_vat += float(items[6].replace(',', ''))
            total_vat +=  float(items[7].replace(',', ''))
            total_amoun +=  float(items[8].replace(',', ''))


    return render_template('Report/vatnoreport.html',
                           from_date=from_date,
                           to_date=to_date,
                           data=data,
                           title=session.get("database"),
                           countdata=countdata,
                           total_befor_vat=total_befor_vat,
                           total_vat=total_vat,
                           total_amoun=total_amoun
                           )


@app.route('/monthly/<id>')
@app.route('/monthly', defaults={'id':''})
def monthly(id):
    if not session.get("name"):
            return redirect("/login") 
    with getcursor(session.get("database")) as cur:
        dateTimeObj = datetime.now()
        day_ = dateTimeObj.strftime("%Y-%m")
        if id=='':
            id=day_ 
        cur.execute("""select to_char(doc_date,'YYYY-MM'),  trim(to_char(sum(total_vat_value),'999G999G999G999')) from ic_trans group by to_char(doc_date,'YYYY-MM')
                        order by to_char(doc_date,'YYYY-MM') DESC limit 12""")
        data = cur.fetchall()
        cur.execute("""SELECT row_number() OVER (order by doc_no) as rnum,doc_no, to_char(doc_date,'DD-MM-YYYY'),b.name_1,remark,trim(to_char(value_before_vat,'999G999G999G999D99')) as value_before_vat,
                            trim(to_char(total_vat_value,'999G999G999G999D99')) as total_vat_value,
                            trim(to_char(total_amount,'999G999G999G999D99')) as total_amount
                            FROM ic_trans a
                            left join ar_customer b on code=a.cust_code
                            left join province c on c.code=b.province
                            left join city d on d.province=b.province and d.code=b.city
                            where trans_flag=44 and to_char(doc_date,'YYYY-MM')=%s
                            order by doc_no """,(id,))
        datadeltail = cur.fetchall()
    # return render_template('Product/index.html',data=data,title=session.get("database"))
    return render_template('Report/monthly/mothly_recipt.html', title=session.get("database"),data=data,datadeltail=datadeltail)