from flask import Flask, render_template, request, redirect, url_for, session,make_response
from app import app
from dbconn import *
import psycopg2 as p
from datetime import datetime, date



@app.route('/contract')
def contract():
    if not session.get("name"):
            return redirect("/login") 
    with getcursor(session.get("database")) as cur:
        cur.execute("select code,name_1 from province order by roworder ASC")
        province = cur.fetchall()

        cur.execute("""
                    SELECT row_number() OVER (ORDER BY a.code) as rnum,a.code, a.name_1, address||', ເມືອງ '||c.name_1||', ເເຂວງ '||b.name_1,a.name_2,address,a.province,a.city,tel,tin FROM ar_customer a
                    left join public.province b on b.code=a.province
                    left join public.city c on c.province=a.province and c.code=a.city
                    """)
        customer = cur.fetchall()
        cur.execute("SELECT max(code) FROM ar_customer ")
        ar_id = cur.fetchone()
        if ar_id[0]:
            ar_code=1+int(ar_id[0])
        else:
            ar_code='100001'
        cur.execute("select code,name_1 from city order by roworder ASC")
        city = cur.fetchall()
    return render_template('contract/index.html',title=session.get("database"),data=customer,province=province,ar_code=ar_code,city=city)


@app.route('/save_cust',methods=['POSt'])
def save_cust():
    if not session.get("name"):
        return redirect("/login")
    try:
        with getcursor(session.get("database")) as cur:
            code = request.form['code']
            name_1 = request.form['name_1']
            name_2 = request.form['name_2']
            address = request.form['address']
            province = request.form['province']
            city = request.form['city']
            tel = request.form['tel']
            tax_id = request.form['tax_id']
            sql = "INSERT INTO ar_customer(code, name_1, name_2, address, city, province, tel, tin)VALUES (%s, %s,%s, %s,%s,%s, %s, %s);"
            data = (code, name_1, name_2, address,city,province,tel,tax_id)
            cur.execute(sql, (data))
    except Exception as e:
        print("error in executing with exception: ", e)
    return redirect(url_for('contract'))

@app.route('/update_cust',methods=['POSt'])
def update_cust():
    if not session.get("name"):
        return redirect("/login")
    try:
        with getcursor(session.get("database")) as cur:
            code = request.form['code']
            name_1 = request.form['name_1']
            name_2 = request.form['name_2']
            address = request.form['address']
            province = request.form['province']
            city = request.form['city']
            tel = request.form['tel']
            tax_id = request.form['tax_id']
            sql = "update  ar_customer set name_1=%s, name_2=%s, address=%s, city=%s, province=%s, tel=%s, tin=%s where code=%s"
            data = (name_1, name_2, address,city,province,tel,tax_id,code)
            cur.execute(sql, (data))
    except Exception as e:
        print("error in executing with exception: ", e)
    return redirect(url_for('contract'))



# @app.route('/deletecontract/<id>')
# def deletecontract(id):
#     with getcursor(session.get("database")) as cur:
#         cur.execute("delete from ar_customer  where code=%s",(id,))
#     return redirect(url_for('contract'))






@app.route('/deletecontract/<id>' ,methods=['DELETE'])
def deletecontract(id):
    with getcursor(session.get("database")) as cur:
        cur.execute("delete from ar_customer  where code=%s",(id,))
    return '', 200



