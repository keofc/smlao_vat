from flask import Flask, render_template, request, redirect, url_for, session,make_response
from app import app
from psycopg2.pool import SimpleConnectionPool
from contextlib import contextmanager


@contextmanager
def getcursorSMLAO():
    dbConnection = "dbname='smlaomanager' user='postgres' host='150.95.90.124' password='sml'"
    # pool define with 10 live connections
    connectionpool = SimpleConnectionPool(1,10,dsn=dbConnection)
    con = connectionpool.getconn()
    con.autocommit = True
    try:
        yield con.cursor()
    finally:
        connectionpool.putconn(con)
