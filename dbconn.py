from flask import Flask, render_template, request, redirect, url_for, session,make_response
from app import app
from psycopg2.pool import SimpleConnectionPool
from contextlib import contextmanager

# dbConnection = "dbname='sabaideevat' user='postgres' host='150.95.90.124' password='sml'"

#     # pool define with 10 live connections
# connectionpool = SimpleConnectionPool(1,10,dsn=dbConnection)

@contextmanager
def getcursor(id):
    dbConnection = "dbname='"+id+"' user='postgres' host='150.95.90.124' password='sml'"
    # pool define with 10 live connections
    connectionpool = SimpleConnectionPool(1,10,dsn=dbConnection)
    con = connectionpool.getconn()
    con.autocommit = True
    try:
        yield con.cursor()
    finally:
        connectionpool.putconn(con)
import sqlite3
conn = sqlite3.connect('smlao.db', check_same_thread=False)
print("เปิดฐานข้อมูลสำเร็จ")


conn.execute('''
        CREATE TABLE IF NOT EXISTS ic_trans_detail
       (roworder INTEGER  PRIMARY KEY AUTOINCREMENT,
        doc_no TEXT,
        cust_code TEXT,
        iteme_code TEXT,
        iteme_name TEXT,
        unit_code TEXT,
        qty double default 0,
        qty2 double default 0,
        discount double default 0, 
        price double  default 0,
        sum_amount double  default 0,
        vat_rate double  default 0, 
        vat_value double  default 0, 
        total_amount double  default 0,
        user_created text,
        trans_flag INTEGER,
        tax_type int );''')
print("สร้างตารางสำเร็จ :D ")

def ic_trans_detail(id):
    with conn:    
        curs = conn.cursor()    
        curs.execute("SELECT row_number() OVER () as rnum,iteme_code,iteme_name,qty,unit_code,price,discount,sum_amount,vat_value,total_amount,user_created,roworder,tax_type FROM ic_trans_detail where user_created=?",(id,))
        rows = curs.fetchall()
    return rows

# def checkdocni(id):
#     with conn:    
#         curs = conn.cursor()    
#         curs.execute("SELECT row_number() OVER () as rnum,iteme_code,iteme_name,qty,unit_code,price,discount,sum_amount,vat_value,total_amount,user_created,roworder FROM ic_trans_detail where doc_no=?",(id,))
#         rows = curs.fetchall()
#         print("id:"+id)
#     return rows

# def cleasqllift(id):
#         print(id,)
#         conn.execute("delete from ic_trans_detail where user_created=? and trans_flag=?", (session.get("name"),id,))
#         conn.commit()
# @app.route('/vatprocess/<id>/<id1>/<id3>')
# def vatprocess(id,id1,id3):
#     print("ffff")
#     countvat(id,id1,id3)
#     if countvat(id,id1,id3)=='complete':
#         if id3=='11':
#             return redirect(url_for('ponew',id=id,id1=id1))
#         elif id3=='30':
#             return redirect(url_for('createqt',id=id,id1=id1))
#         elif id3=='40':
#             return redirect(url_for('createSO',id=id,id1=id1))






# def countvat(id,id1,id3):
#     if id1 == '0':
#         conn.execute("update ic_trans_detail set vat_value=0, total_amount=sum_amount+0 where user_created=? and trans_flag=?", (session.get("name"),id3,))
#     elif  id1 == '1':
#         conn.execute("update ic_trans_detail set vat_value=sum_amount*(0.07), total_amount=sum_amount+(sum_amount*(0.07)) where user_created=? and trans_flag=?", (session.get("name"),id3,))
#     else:
#          conn.execute("update ic_trans_detail set vat_value=(sum_amount/(0.93))-sum_amount,total_amount=sum_amount where user_created=? and trans_flag=?", (session.get("name"),id3,))
#     conn.commit()
#     return 'complete'


# import psycopg2 as p

# try:
#     con = p.connect(host = "localhost",database="ben01",user="postgres",password="postgres",)

#     cur = con.cursor()
#     cur.execute('SELECT version()')
#     ver = cur.fetchone()
#     print(ver)
# except p.DatabaseError:
#     print('Error %s'%p.DatabaseError)
# if con:
#     con.close()